
package edu.uprm.cse.datastructures.cardealer;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarList;
import edu.uprm.cse.datastructures.cardealer.util.CircularSortedDoublyLinkedList;

@Path("/cars")
public class CarManager {

	private final CircularSortedDoublyLinkedList<Car> carList = CarList.getInstance();
	
	//RETURNS LIST OF CARS
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Car[] getAllCars(){
		Car[] allCars = new Car[this.carList.size()];
		if(this.carList.isEmpty()) {
			return allCars;
		}
		for(int i= 0 ; i<this.carList.size(); i++) {
			allCars[i] = this.carList.get(i);
		}
		return allCars;	
	}
	//ID RELATED CODES. 
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Car getCar(@PathParam("id") int id) {
		for(int i=0; i<this.carList.size();i++) {
			if(this.carList.get(i).getCarId() == id) {
				return this.carList.get(i);
			}
		}
		throw new WebApplicationException(404);

	}
	@PUT
	@Path("/{id}/update")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCar(@PathParam("id") int id, Car car) {
		for(int i = 0; i<this.carList.size();i++) {
			if(this.carList.get(i).getCarId() == (id)){
				this.carList.remove(this.carList.get(i));
				this.carList.add(car);

				return Response.status(Response.Status.OK).build();
			}

		}
		//If not found
		return Response.status(Response.Status.NOT_FOUND).build();

	}
	//Delete
	@DELETE
	@Path("/{id}/delete")
	public Response deleteCar(@PathParam("id") int id) {
		for(int i=0; i<this.carList.size();i++) {
			if(this.carList.get(i).getCarId()== id) {
				this.carList.remove(this.carList.get(i));
				return Response.status(Response.Status.OK).build();
			}
		}
		//If not found
		return Response.status(Response.Status.NOT_FOUND).build();

	}
	//ADD
	@POST
	@Path("/add")
	@Produces(MediaType.APPLICATION_JSON)
	public Response addCar(Car car) {
		for(int i=0; i<this.carList.size(); i++) {
			if(this.carList.get(i).getCarId() == car.getCarId()) {
				return Response.status(Response.Status.FORBIDDEN).build(); //Prevents adding an existing car. 
			}
		}
		//If it is not already on the list, add. 
		this.carList.add(car);
		return Response.status(201).build();

	}

}
