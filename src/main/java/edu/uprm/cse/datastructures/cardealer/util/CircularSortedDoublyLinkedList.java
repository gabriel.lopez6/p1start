package edu.uprm.cse.datastructures.cardealer.util;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	private Node<E> header = new Node<E>(null, null, null);
	private int size;
	private Comparator<E> comparator;

	public CircularSortedDoublyLinkedList(Comparator<E> comparator) {
		this.comparator = comparator;

	}

	private static class Node<E>{
		private E element;	
		private Node<E> next; 
		private Node<E> prev;

		Node(E element, Node<E> next, Node<E> prev){
			this.element = element; 
			this.next = next; 
			this.prev = prev; 

		}

		private E getElement() {
			return element; 
		}

		private Node<E> getNext(){
			return next;
		}

		private Node<E> getPrev(){
			return prev;
		}
		private void setElement(E element) {
			this.element = element;
		}
		private void setNext(Node<E> next) {
			this.next = next;
		}
		private void setPrev(Node<E> prev) {
			this.prev = prev;
		}
	}

	private class CircularSortedDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextOne;


		public CircularSortedDoublyLinkedListIterator() {
			this.nextOne = (Node<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return nextOne != header;
		}

		@Override
		public E next() {
			if(this.hasNext()) {

				E nextElement = this.nextOne.getElement();
				this.nextOne = this.nextOne.getNext(); //Increment 

				return nextElement; 
			}
			return null;
		}
	}
	@Override
	public Iterator<E> iterator() {	return new CircularSortedDoublyLinkedListIterator<E>();}

	@Override
	public boolean add(E obj) {
		if(this.isEmpty()){
			Node<E> node = new Node<E>(obj,this.header,this.header);
			this.header.setNext(node);
			this.header.setPrev(node);
		}
		else {
			Node<E> temp = this.header.getNext();
			while((comparator.compare(obj, temp.getElement())> 0)) {
				temp = temp.getNext();
				if(temp.equals(this.header))
					break;
			}
			Node<E> node = new Node<>(obj, temp, temp.getPrev());
			temp.getPrev().setNext(node);
			temp.setPrev(node);
		}
		this.size++;
		return true;
	}

	@Override
	public int size() {
		return this.size;
	}

	@Override
	public boolean remove(E obj) {
		Node<E> node = this.header.getNext();
		if(this.isEmpty()) {
			return false;
		}
		while(node!=this.header) {
			if(node.getElement().equals(obj)) {
				node.getNext().setPrev(node.getPrev());
				node.getPrev().setNext(node.getNext());
				node.setElement(null);
				node.setNext(null);
				node.setPrev(null);
				this.size--;
				return true;
			}else {
				node=node.getNext();
			}
		}
		return false;

	}

	@Override
	public boolean remove(int index) {
		if(this.isEmpty()) {
			return false; 
		}
		if(index <0 || index > this.size-1) {

			throw new IndexOutOfBoundsException("Please choose a valid index");
		}
		//Use previous method to make this one easier. 
		return this.remove(this.get(index));
	}

	@Override
	public int removeAll(E obj) {
		int counter = 0;
		while(this.remove(obj)) {
			counter++;
		}
		return counter;
	}

	@Override
	public E first() { return (this.isEmpty() ? null : this.header.getNext().getElement());}

	@Override
	public E last() { return(this.isEmpty() ? null : this.header.getPrev().getElement()); }

	@Override
	public E get(int index) {
		Node<E> temp = this.header.getNext();
		int i = 0;
		
		if(this.isEmpty()) {
			return null;
		}
		if(index<0 || index>this.size-1) {
			return null;
		}
		while(temp != this.header) {
			if(i == index) {
				return temp.getElement();
			}
			else {

				temp = temp.getNext();
				i++;
			}
		}
		return temp.getNext().getElement();
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}

	}

	@Override //If index 0 or bigger, means it exists. 
	public boolean contains(E e) { return this.firstIndex(e)>=0;}

	@Override
	public boolean isEmpty() { return this.size==0;}

	@Override
	public int firstIndex(E e) {
		int firstIndex = 0;
		for(Node<E> i = this.header.getNext(); i != this.header; i = i.getNext() ) {
			if(i.getElement().equals(e)) {
				return firstIndex;
			}
			else {
				firstIndex++;
			}
		}

		return -1;
	}

	@Override
	public int lastIndex(E e) {
		int lastIndex = this.size-1;
		for(Node<E> node = this.header.getPrev(); node != this.header; node = node.getPrev()) {
			if(node.getElement().equals(e)) {
				return lastIndex;
			}
			else {
				lastIndex--;
			}
		}
		return -1;
	}

}
