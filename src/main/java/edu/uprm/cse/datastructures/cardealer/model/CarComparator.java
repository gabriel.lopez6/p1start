package edu.uprm.cse.datastructures.cardealer.model;

import java.util.Comparator;

public class CarComparator implements Comparator<Car> {
 //Puts together brand, model and model option as a 
 //	string to compare if they are equal or not.
	@Override
	public int compare(Car car1, Car car2) {
	
		String firstCar = car1.getCarBrand() + car1.getCarModel() + car1.getCarModelOption();
		String secondCar = car2.getCarBrand() + car2.getCarModel() + car2.getCarModelOption();
		
		return firstCar.compareTo(secondCar);
	}

}
